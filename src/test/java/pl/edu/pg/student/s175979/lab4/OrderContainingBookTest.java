/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pg.student.s175979.lab4;

/**
 *
 * @author makiki
 */

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import org.mockito.junit.MockitoJUnitRunner;
import pl.edu.pg.student.s175979.lab4.model.Book;
import pl.edu.pg.student.s175979.lab4.model.Order;
import pl.edu.pg.student.s175979.lab4.services.OrdersService;

@RunWith(MockitoJUnitRunner.class)
public class OrderContainingBookTest {
    
    
    @Test
    public void whenNoSuitableOrderFound_returnEmptySetOfOrders() {
        EntityManager em = Mockito.mock(EntityManager.class, RETURNS_DEEP_STUBS);
        Book b = new Book();
        b.setAmount(1);
        Order o = new Order();
        List<Order> l = new ArrayList<>();
        Mockito.when(em.createQuery("SELECT o FROM Order o", Order.class).getResultList()).thenReturn(l);
        OrdersService os = new OrdersService(em);
        
        l.add(o);
        
        l = os.findContainingBook(b);
        Assert.assertTrue(l.isEmpty());

    }
    
    @Test
    public void whenASuitableOrderIsFound_returnSetContainingIt() {
        EntityManager em = Mockito.mock(EntityManager.class, RETURNS_DEEP_STUBS);
        Book b = new Book();
        b.setAmount(1);
        Order o = new Order();
        o.getBooks().add(b);
        List<Order> l = new ArrayList<>();
        Mockito.when(em.createQuery("SELECT o FROM Order o", Order.class).getResultList()).thenReturn(l);
        OrdersService os = new OrdersService(em);
        
        l.add(o);
        
        l = os.findContainingBook(b);
        System.err.println(l.size());
        Assert.assertTrue(l.contains(o));
    }
    
    @Test
    public void whenMultipleOrdersAreFound_returnSetContainingAllOfThem() {
        EntityManager em = Mockito.mock(EntityManager.class, RETURNS_DEEP_STUBS);
        Book b = new Book();
        b.setAmount(2);
        Order o1 = new Order();
        o1.getBooks().add(b);
        Order o2 = new Order();
        o2.getBooks().add(b);
        List<Order> l = new ArrayList<>();
        Mockito.when(em.createQuery("SELECT o FROM Order o", Order.class).getResultList()).thenReturn(l);
        OrdersService os = new OrdersService(em);
        
        l.add(o1);
        l.add(o2);
        
        l = os.findContainingBook(b);
        Assert.assertTrue(l.contains(o1) && l.contains(o2) && l.size() == 2);
    }
    
    @Test
    public void whenMultipleOrdersAvailableWithOnlyOneSuitable_returnOnlySuitable() {
        EntityManager em = Mockito.mock(EntityManager.class, RETURNS_DEEP_STUBS);
        Book b = new Book();
        b.setAmount(1);
        Order o1 = new Order();
        o1.getBooks().add(b);
        Order o2 = new Order();
        List<Order> l = new ArrayList<>();
        Mockito.when(em.createQuery("SELECT o FROM Order o", Order.class).getResultList()).thenReturn(l);
        OrdersService os = new OrdersService(em);
        
        l.add(o1);
        l.add(o2);
        
        l = os.findContainingBook(b);
        Assert.assertTrue(l.contains(o1) && l.size() == 1);
    }
}
