/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pg.student.s175979.lab4;

import javax.persistence.EntityManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.edu.pg.student.s175979.lab4.model.Book;
import pl.edu.pg.student.s175979.lab4.model.Order;
import pl.edu.pg.student.s175979.lab4.services.OrdersService;
import pl.edu.pg.student.s175979.lab4.services.exceptions.OutOfStockException;
import pl.edu.pg.student.s175979.lab4.services.exceptions.TooDangerousException;

/**
 *
 * @author makiki
 */
@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest {
    @Mock
    EntityManager em;
    
    @Test(expected = OutOfStockException.class)
    public void whenBookOrderedNotAvailable_OutOfStockExceptionThrown() {
        Order o = new Order();
        Book b = new Book();
        b.setAmount(0);
        o.getBooks().add(b);
        Mockito.when(em.find(Book.class, b.getId())).thenReturn(b);
        OrdersService os = new OrdersService(em);
        
        os.placeOrder(o);
    }
    
    @Test(expected = OutOfStockException.class)
    public void whenOneBookOrderedNotAvailableButOtherIs_OutOfStockExceptionThrown() {
        Order o = new Order();
        Book b1 = new Book();
        b1.setAmount(1);
        Book b2 = new Book();
        b2.setAmount(0);
        o.getBooks().add(b1);
        o.getBooks().add(b2);
        Mockito.when(em.find(Book.class, b1.getId())).thenReturn(b1);
        Mockito.when(em.find(Book.class, b2.getId())).thenReturn(b2);
        OrdersService os = new OrdersService(em);
        
        os.placeOrder(o);
    }
    
    @Test(expected = TooDangerousException.class)
    public void whenBookOrderedTooDangerous_TooDangerousExceptionThrown() {
        Order o = new Order();
        Book b = new Book();
        b.setAmount(1);
        b.setDanger(15);
        o.getBooks().add(b);
        Mockito.when(em.find(Book.class, b.getId())).thenReturn(b);
        OrdersService os = new OrdersService(em);
        
        os.placeOrder(o);
    }
    
    @Test(expected = TooDangerousException.class)
    public void whenAllBooksOrderedTooDangerous_TooDangerousExceptionThrown() {
        Order o = new Order();
        Book b1 = new Book();
        b1.setAmount(1);
        b1.setDanger(3);
        Book b2 = new Book();
        b2.setAmount(1);
        b2.setDanger(6);
        o.getBooks().add(b1);
        o.getBooks().add(b2);
        Mockito.when(em.find(Book.class, b1.getId())).thenReturn(b1);
        Mockito.when(em.find(Book.class, b2.getId())).thenReturn(b2);
        OrdersService os = new OrdersService(em);
        
        os.placeOrder(o);
    }
    
    @Test
    public void whenOrderSucceeds_BookAmountDecreasesByOne() {
        Order o = new Order();
        Book b = new Book();
        b.setAmount(1);
        b.setDanger(0);
        o.getBooks().add(b);
        Mockito.when(em.find(Book.class, b.getId())).thenReturn(b);
        OrdersService os = new OrdersService(em);
        
        os.placeOrder(o);
        
        Assert.assertEquals(0, (int)b.getAmount());
        Mockito.verify(em, Mockito.times(1)).persist(o);
    }
}
