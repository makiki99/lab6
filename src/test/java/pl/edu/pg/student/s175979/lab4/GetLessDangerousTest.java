/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pg.student.s175979.lab4;

import java.util.ArrayList;
import java.util.List;
import pl.edu.pg.student.s175979.lab4.model.Book;
import javax.persistence.EntityManager;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import org.mockito.junit.MockitoJUnitRunner;
import pl.edu.pg.student.s175979.lab4.services.BooksService;

/**
 *
 * @author corpus
 */
@RunWith(MockitoJUnitRunner.class)
public class GetLessDangerousTest {
    
    
    
    @Test
    public void LessDangerousBookIsReturned()
    {
        EntityManager em = Mockito.mock(EntityManager.class, RETURNS_DEEP_STUBS);
        Book b = new Book();
        b.setDanger(5);
        List<Book> ls = new ArrayList<>();
        ls.add(b);
        Mockito.when(em.createNamedQuery(Book.FIND_ALL, Book.class).getResultList()).thenReturn(ls);
        BooksService bs= new BooksService(em);
        
        List<Book> lb=bs.findBelowDanger(6);
        Assert.assertTrue(lb.contains(b));
    }
    
    @Test
    public void MoreDangerousBookIsNotReturned()
    {
        EntityManager em = Mockito.mock(EntityManager.class, RETURNS_DEEP_STUBS);
        Book b = new Book();
        b.setDanger(5);
        List<Book> ls = new ArrayList<>();
        ls.add(b);
        Mockito.when(em.createNamedQuery(Book.FIND_ALL, Book.class).getResultList()).thenReturn(ls);
        BooksService bs= new BooksService(em);
        
        List<Book> lb=bs.findBelowDanger(5);
        Assert.assertTrue(lb.isEmpty());
    }
    
    @Test
    public void MoreDangerousBookIsNotReturnedAndLessDangerousBookIsReturned()
    {
        EntityManager em = Mockito.mock(EntityManager.class, RETURNS_DEEP_STUBS);
        Book b = new Book();
        b.setDanger(5);
        Book c = new Book();
        c.setDanger(4);
        List<Book> ls = new ArrayList<>();
        ls.add(b);
        ls.add(c);
        Mockito.when(em.createNamedQuery(Book.FIND_ALL, Book.class).getResultList()).thenReturn(ls);
        BooksService bs= new BooksService(em);
        
        List<Book> lb=bs.findBelowDanger(5);
        boolean cor=((lb.contains(c))&&(!lb.contains(b)));
        Assert.assertTrue(cor);
    }

    @Test
    public void MultipleLessDangerousBooksAreStillReturned()
    {
        EntityManager em = Mockito.mock(EntityManager.class, RETURNS_DEEP_STUBS);
        Book b = new Book();
        b.setDanger(5);
        Book c = new Book();
        c.setDanger(4);
        List<Book> ls = new ArrayList<>();
        ls.add(b);
        ls.add(c);
        Mockito.when(em.createNamedQuery(Book.FIND_ALL, Book.class).getResultList()).thenReturn(ls);
        BooksService bs= new BooksService(em);
        
        List<Book> lb=bs.findBelowDanger(6);
        boolean cor=((lb.contains(c))&&(lb.contains(b)));
        Assert.assertTrue(cor);
    }
    
    @Test
    public void MultipleMoreDangerousBooksAreStillNotReturned()
    {
        EntityManager em = Mockito.mock(EntityManager.class, RETURNS_DEEP_STUBS);
        Book b = new Book();
        b.setDanger(5);
        Book c = new Book();
        c.setDanger(4);
        List<Book> ls = new ArrayList<>();
        ls.add(b);
        ls.add(c);
        Mockito.when(em.createNamedQuery(Book.FIND_ALL, Book.class).getResultList()).thenReturn(ls);
        BooksService bs= new BooksService(em);
        
        List<Book> lb=bs.findBelowDanger(4);
        Assert.assertTrue(lb.isEmpty());
    }
    
}
