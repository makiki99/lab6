package pl.edu.pg.student.s175979.lab4.services.exceptions;

/**
 * Wyjątek sygnalizujący niedostępność towaru.
 *
 * Wystąpienie wyjątku z hierarchii RuntimeException w warstwie biznesowej
 * powoduje wycofanie transakcji (rollback).
 */
public class OutOfStockException extends RuntimeException {
}
