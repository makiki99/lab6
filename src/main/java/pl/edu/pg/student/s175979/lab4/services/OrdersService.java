package pl.edu.pg.student.s175979.lab4.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import pl.edu.pg.student.s175979.lab4.model.Book;
import pl.edu.pg.student.s175979.lab4.model.Order;
import pl.edu.pg.student.s175979.lab4.services.exceptions.OutOfStockException;
import pl.edu.pg.student.s175979.lab4.services.exceptions.TooDangerousException;

/**
 * Komponent (serwis) biznesowy do realizacji operacji na zamówieniach.
 */
@Service
public class OrdersService extends EntityService<Order> {

    //Instancja klasy EntityManger zostanie dostarczona przez framework Spring
    //(wstrzykiwanie zależności przez konstruktor).
    public OrdersService(EntityManager em) {

        //Order.class - klasa encyjna, na której będą wykonywane operacje
        //Order::getId - metoda klasy encyjnej do pobierania klucza głównego
        super(em, Order.class, Order::getId);
    }

    /**
     * Pobranie wszystkich zamówień z bazy danych.
     *
     * @return lista zamówień
     */
    public List<Order> findAll() {
        return em.createQuery("SELECT o FROM Order o", Order.class).getResultList();
    }
    
    public List<Order> findContainingBook(Book b) {
         List<Order> ret = findAll();
         ret.removeIf((t) -> {
             return !(t.getBooks().contains(b)); 
         });
         return ret;
    }
    

    @Transactional
    public void placeOrder(Order order) {
        int totalDanger = 0;
        for (Book bookStub : order.getBooks()) {
            Book book = em.find(Book.class, bookStub.getId());

            if (book.getAmount() < 1) {
                //wyjątek z hierarchii RuntineException powoduje wycofanie transakcji (rollback)
                throw new OutOfStockException();
            } else {
                int newAmount = book.getAmount() - 1;
                totalDanger += book.getDanger();
                if (totalDanger > 8) {
                    throw new TooDangerousException();
                }
                book.setAmount(newAmount);
            }
        }

        //jeśli wcześniej nie został wyrzucony wyjątek OutOfStockException, zamówienie jest zapisywane w bazie danych
        save(order);
    }
}
